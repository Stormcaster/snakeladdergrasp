package com.ske.snakebaddesign.models;

/**
 * Created by zBlizzarDz on 15-Mar-16.
 */
public class Player {
    private Piece piece;
    private String name;

    public Player(String name) {
        this.name = name;
        piece = new Piece();
    }

    public Piece getPiece() {
        return piece;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
