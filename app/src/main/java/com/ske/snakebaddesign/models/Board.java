package com.ske.snakebaddesign.models;

import java.util.ArrayList;

/**
 * Created by zBlizzarDz on 15-Mar-16.
 */
public class Board {
    private int size;
    private ArrayList<Square> squares;

    public Board(int size) {
        this.size = size;
        squares = new ArrayList<Square>();
        for(int i = 0; i < size*size; i++) {
            if(i % 10 == 0 && i != 0) squares.add(new WarpSquare(i));
            else squares.add(new NormalSquare(i));
        }
    }

    public int getSize() {
        return size;
    }

    public Square getSquares(int num) {
        return squares.get(num);
    }
}
