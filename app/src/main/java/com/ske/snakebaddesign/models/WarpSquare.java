package com.ske.snakebaddesign.models;

import android.util.Log;

import java.util.Random;

/**
 * Created by zBlizzarDz on 17-Mar-16.
 */
public class WarpSquare extends Square{
    public WarpSquare(int number) {
        super(number);
    }

    @Override
    public int activate(int pos, int boardSize) {
        int rand = 1 + new Random().nextInt(boardSize * boardSize);
        Log.e("Square Effect", "Warp " + rand);
        return rand;
    }
}
