package com.ske.snakebaddesign.models;

import android.util.Log;

import java.util.Observable;
import java.util.Random;

/**
 * Created by zBlizzarDz on 15-Mar-16.
 */
public class Game extends Observable{
    private Player player1, player2;
    private Board board;
    private int turn;

    public Game(Player player1, Player player2, int boardSize) {
        this.player1 = player1;
        this.player2 = player2;
        this.board = new Board(boardSize);
        turn = 0;
    }
    public void advanceTurn() {
        turn++;
    }

    public static int roll() {
        return 1 + new Random().nextInt(6);
    }

    public Player getTurnPlayer() {
        if(turn % 2 == 0) return player1;
        else return player2;
    }

    public Board getBoard() {
        return board;
    }

//    public void reset() {
//        player1.getPiece().setPosition(0);
//        player2.getPiece().setPosition(0);
//        turn = 0;
//    }
    public int adjustPosition(int current, int distance) {
        current = current + distance;
        int maxSquare = this.board.getSize() * this.board.getSize() - 1;
        if(current > maxSquare) {
            current = maxSquare - (current - maxSquare);
        }
        return current;
    }
    public void checkWin() {
        if (player1.getPiece().getPosition() == this.board.getSize() * this.board.getSize() - 1) {
            this.setChanged();
            this.notifyObservers(player1);
            Log.e("Observed", "Detected victory for Player 1");
        } else if (player2.getPiece().getPosition() == this.board.getSize() * this.board.getSize() - 1) {
            this.setChanged();
            this.notifyObservers(player2);
            Log.e("Observed", "Detected victory for Player 2");
        } else {

        }
    }

    public void moveCurrentPiece(int value) {
        if (turn % 2 == 0) {
            int p1position = player1.getPiece().getPosition();
            player1.getPiece().setPosition(adjustPosition(p1position, value));

            p1position = player1.getPiece().getPosition();
            int squareEffect = board.getSquares(p1position).activate(p1position, board.getSize());
            Log.e("Square Effect", "Effect : +" + squareEffect + " at " + p1position);
            player1.getPiece().setPosition(adjustPosition(p1position, squareEffect));
        } else {
            int p2position = player2.getPiece().getPosition();
            player2.getPiece().setPosition(adjustPosition(p2position, value));

            p2position = player2.getPiece().getPosition();
            int squareEffect = board.getSquares(p2position).activate(p2position, board.getSize());
            Log.e("Square Effect", "Effect : +" + squareEffect + " at " + p2position);
            player2.getPiece().setPosition(adjustPosition(p2position, squareEffect));
        }
    }

}
