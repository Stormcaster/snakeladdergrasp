package com.ske.snakebaddesign.models;

/**
 * Created by zBlizzarDz on 15-Mar-16.
 */
public class Piece {
    private int position;

    public Piece() {
        this.position = 0;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
