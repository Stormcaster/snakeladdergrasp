package com.ske.snakebaddesign.models;

/**
 * Created by zBlizzarDz on 15-Mar-16.
 */
public abstract class Square {
    int number;

    public Square(int number) {
        this.number = number;
    }
    public abstract int activate(int pos, int boardSize);

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
