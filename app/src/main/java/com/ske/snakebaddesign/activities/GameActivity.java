package com.ske.snakebaddesign.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ske.snakebaddesign.R;
import com.ske.snakebaddesign.guis.BoardView;
import com.ske.snakebaddesign.models.Game;
import com.ske.snakebaddesign.models.Player;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

public class GameActivity extends AppCompatActivity implements Observer {
    private int boardSize;
    private Game game;
    private BoardView boardView;
    private Button buttonTakeTurn;
    private Button buttonRestart;
    private TextView textPlayerTurn;
    private Player p1, p2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        initComponents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        resetGame();
    }

    private void initComponents() {
        boardView = (BoardView) findViewById(R.id.board_view);
        buttonTakeTurn = (Button) findViewById(R.id.button_take_turn);
        buttonTakeTurn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeTurn();
            }
        });
        buttonRestart = (Button) findViewById(R.id.button_restart);
        buttonRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetGame();
            }
        });
        textPlayerTurn = (TextView) findViewById(R.id.text_player_turn);
    }

    private void resetGame() {
        p1 = new Player("Player 1");
        p2 = new Player("Player 2");
        game = new Game(p1, p2, 6);
        game.addObserver(this);
        updatePosition();
        boardView.setBoardSize(game.getBoard().getSize());
        textPlayerTurn.setText(game.getTurnPlayer().getName() + "'s Turn");
    }
    private void updatePosition() {
        boardView.setP1Position(p1.getPiece().getPosition());
        boardView.setP2Position(p2.getPiece().getPosition());
    }
    private void takeTurn() {
        final int value = Game.roll();
        String title = game.getTurnPlayer().getName() + " rolled a die";
        String msg = game.getTurnPlayer().getName() + " got " + value;
        OnClickListener listener = new OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                game.moveCurrentPiece(value);
                updatePosition();
                game.checkWin();
                game.advanceTurn();
                textPlayerTurn.setText(game.getTurnPlayer().getName() + "'s Turn");
                dialog.dismiss();
            }
        };
        displayDialog(title, msg, listener);
    }



    private void displayDialog(String title, String message, DialogInterface.OnClickListener listener) {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", listener);
        alertDialog.show();
    }

    @Override
    public void update(Observable observable, Object data) {
        Log.e("Observed","UPDATE!");
        if(data instanceof Player) {
            Player winner = (Player)data;
            String title = "Game Over";
            OnClickListener listener = new OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    resetGame();
                    dialog.dismiss();
                }
            };
            if (winner != null) displayDialog(title, winner.getName() + " wins!", listener);
        }
    }
}
